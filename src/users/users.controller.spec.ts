import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import {  User } from './user.entity'

describe('UsersController', () => {
  let controller: UsersController;
  let fakeAuthService: Partial<AuthService>;
  let fakeUsersService: Partial<UsersService>;

  beforeEach(async () => {
    fakeUsersService = {
      findOne: (id:number)=> {return Promise.resolve({id: id,email:"test@test.fr",password:"test"} as User)},
      find : (email : string)=> {return Promise.resolve([{id: 1,email:email,password:"test"} as User])},
      // remove: ()=> {},
      // update: ()=> {}
    }
    fakeAuthService = {
      // signin: () => {},
      // signup: () => {}
    }
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { 
          provide: UsersService,
          useValue: fakeUsersService
        },
        {
          provide: AuthService,
          useValue: fakeAuthService
        }
      ]
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
